#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_LENGTH 64

int ReadWords(char * string, char*** dest) {
	int wordStart, wordEnd, i = 0, num = 0;
	while (string[i] != 0 && string[i] != '\n') {
		if (string[i] != ' ') {
			wordStart = i;
			while (string[i] != 0 && string[i] != '\n' && string[i] != ' ') i++;
			wordEnd = --i;
			num++;
			(*dest) = (char**)realloc((*dest), num*sizeof(char*));
			(*dest)[num - 1] = (char*)calloc(wordEnd - wordStart + 2, sizeof(char));
			for (int j = wordStart; j <= wordEnd; j++)
				(*dest)[num - 1][j - wordStart] = string[j];
		}
		i++;
	}
	return num;
}

void PrintString(char * string)
{
	printf("%s ", string);
}

void RandomizeString(char * str)
{
	int i = 0, num = 0;
	char** words = (char**)calloc(num, sizeof(char*));
	num = ReadWords(str, &words);

	while (num) {
		int random = rand() % num;
		PrintString(words[random]);
		free(words[random]); // �������� �� ���� ����������� ������
		words[random] = words[--num]; // ������������ ��������� �� ��������� �������
	}
	printf("\n");
	free(words);
}

int main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");

	FILE* fp = fopen("D:\\����\\text.txt", "r");
	if (fp == NULL) {
		printf("������! ���� �� ������!\n");
		return 1;
	}
	char string[MAX_LENGTH];
	while (fgets(string, MAX_LENGTH, fp)) {
		RandomizeString(string);
	}
	return 0;
}